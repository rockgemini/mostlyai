# UI and API Test Automation Framework based on [Playwright](https://github.com/microsoft/playwright/)

## Supported Browsers
- Chromium
- Mozilla Firefox
- WebKit (Safari)

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Running Tests](#running-tests)
- [Linting](#linting)
- [Directory Structure](#directory-structure)
- [Local Docker installation](#local-docker-installation)
- [Building docker container](#building-docker-container)
- [Running Playwright tests with Docker](#running-playwright-tests-with-docker)
- [Cleaning and stopping the container before tests run](#cleaning-and-stopping-the-container-before-tests-run)
- [Copying Playwright test results into local (host) space](#copying-playwright-test-results-into-local-(host)-space)
- [Test Video](#test-video)

## Prerequisites

Before running the tests, make sure you have the following installed on your machine:

- [Node.js](https://nodejs.org)
- [pnpm](https://pnpm.io/)

You can verify successful installation by running the following commands:

```bash
node -v
pnpm -v
```

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/rockgemini/mostlyai.git
   cd mostlyai
   ```
2. Install project dependencies using pnpm:
    ```bash
    pnpm install
    ```
## Running Tests
To run the tests, you can use the following npm scripts:
- Run tests in headless mode:
    ```bash
    pnpm test
    ```
- Run tests in headed mode (showing the Chrome browser UI):
    ```bash
    pnpm test:headed
    ```
## Linting
- To lint the project using XO, run the following command:
    ```bash
    pnpm lint
    ```

## Directory Structure
- husky: Husky configuration for Git hooks.
- node_modules: Node.js dependencies (auto-generated, do not commit).
- playwright-report: Report directory for Playwright tests (auto-generated, do not commit).
- src: Source code directory.
- test-results: Test results directory (auto-generated, do not commit).
- .gitignore: Git ignore rules.
- .xo-config.json: XO (eslint) configuration.
- package.json: Project metadata and dependencies.
- playwright.config.ts: Playwright configuration file.
- pnpm-lock.yaml: pnpm package lock file.
- tsconfig.json: TypeScript configuration.

## Local Docker installation

Docker Desktop should be installed and running on the local machine.
To install Docker Desktop locally, please [follow](https://www.docker.com/products/docker-desktop/)

Project docker script file is named as `Dockerfile`, which is placed in the root directory

## Building docker container

To build the docker container for Playwright framework after project is already cloned, please execute

```bash
docker build -t playwright-test-image .
```

Docker will find `Dockerfile` in the current directory and build the image by following the instructions inside `Dockerfile`. The `-t` flag tags the Docker image by naming it `playwright-test-image`. The `.` tells Docker to look for Dockerfile in this current directory. You can review the Docker [product docs](https://docs.docker.com/engine/reference/commandline/build/) for more about building Docker images.

Check if the Docker image was created successfully:

```bash
docker image ls
```

Then the result should be something like this

```bash
REPOSITORY                         TAG                    IMAGE ID       CREATED          SIZE
playwright-test-image              latest                 6f506335f2e2   45 seconds ago   1.85GB
<none>                             <none>                 1d213fddee36   6 days ago       1.85GB
mcr.microsoft.com/playwright       v1.29.2-jammy          88f0ccf642e9   3 weeks ago      1.69GB
```

## Running Playwright tests with Docker

To run the test suite for Consumer Portal in Chrome inside Docker container, please execute

```bash
docker run --name ui-test-runner playwright-test-image pnpm test
```
Where `ui-test-runner` is the name of the container that it creates and `test` is the test script from package.json which runs the test in chrome browser in default headless mode.

## Cleaning and stopping the container before tests run

Please note, you would need to remove the container after each run, stopping is optional.

To stop the docker container, please run the command

```bash
docker stop ui-test-runner
```

To clean the container, please run the command

```bash
docker rm ui-test-runner
```

## Copying Playwright test results into local (host) space

To copy report files from the container to local (host) machine, please run

```bash
docker cp ui-test-runner:/app/playwright-report/ ./playwright-report
```

## Test Video

Currently, a video of a test will be recorded and will be available under `test-results` folder only when it retries a test based on this configuration in `playwright.config.ts`. If you wish to generate video for each run, change the value to `on`.

```bash
video: 'on-first-retry'
```
