FROM mcr.microsoft.com/playwright:v1.36.2

# Set the working directory for the application
WORKDIR /app

# Copy the files containing packages and dependencies into the working directory
COPY ["package.json", "pnpm-lock.yaml", "./"]

# Install dependencies
RUN npm install -g pnpm
RUN npm pkg delete scripts.prepare
RUN pnpm install

# Copy the source code into the Docker image
COPY . .