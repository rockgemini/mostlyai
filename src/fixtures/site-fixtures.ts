import { test as baseTest } from '@playwright/test';
import { HomePage } from 'src/pages/home-page';
import { SearchResultsPage } from 'src/pages/search-results-page';
import { ContactPage } from 'src/pages/contact-page';

type portalFixtures = {
	homePage: HomePage;
	searchResultsPage: SearchResultsPage;
	contactPage: ContactPage;
};

export const test = baseTest.extend<portalFixtures>({
	async homePage({ page }, use) {
		const homePage = new HomePage(page);
		await use(homePage);
	},

	async searchResultsPage({ page }, use) {
		const searchResultsPage = new SearchResultsPage(page);
		await use(searchResultsPage);
	},

	async contactPage({ page }, use) {
		const contactPage = new ContactPage(page);
		await use(contactPage);
	},
});

export { expect } from '@playwright/test';
