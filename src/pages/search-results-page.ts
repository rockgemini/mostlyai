import type { Locator, Page } from '@playwright/test';

export class SearchResultsPage {
	readonly noSearchResult: Locator
	readonly searchedText: Locator

	constructor(readonly page: Page) {
		this.noSearchResult = page.locator('h1.ct-headline');
		this.searchedText = page.locator('h1.ct-code-block ');
	};

	async returnSearchResultText() {
		await this.noSearchResult.waitFor({ state: 'visible' });
		return (await this.noSearchResult.innerText()).trim() + ' ' + (await this.searchedText.innerText()).trim();
	};
};
