import type { Locator, Page } from '@playwright/test';

export interface FormData {
    firstName: string;
    lastName: string;
    email: string;
    mobilePhone: string;
    organization: string;
    country: string;
    referralSource: string;
    message: string;
};

export class ContactPage {
	readonly url: string; 
    readonly formHeader: Locator;
    readonly firstName: Locator;
	readonly lastName: Locator ;
    readonly email: Locator;
    readonly mobilePhone: Locator;
    readonly organization: Locator;
    readonly country: Locator;
    readonly referralSource: Locator;
    readonly messageBox: Locator;
    readonly marketingOffersCheckbox: Locator;
    readonly sendMessageButton: Locator;

	constructor(readonly page: Page) {
		this.url = 'https://mostly.ai/contact';
        this.formHeader = page.getByRole('heading', { name: 'Ask us anything!' });
        this.firstName = page.getByLabel('First Name');
		this.lastName = page.getByLabel('Last Name');
        this.email = page.getByLabel('Your Business Email');
        this.mobilePhone = page.getByLabel('Mobile phone number');
        this.organization = page.getByLabel('Your Organization');
        this.country = page.locator("//select[@name='country']");
        this.referralSource = page.getByLabel('How did you hear about MOSTLY AI?*');
        this.messageBox = page.getByPlaceholder('How can we help you?');
        this.marketingOffersCheckbox = page.getByLabel('Marketing offers and updates.*');
        this.sendMessageButton = page.getByRole('button', { name: 'SEND MESSAGE' });
	};

    async fillContactForm(formData: FormData) {
        await this.formHeader.click();
        await this.firstName.fill(formData.firstName);
        await this.lastName.fill(formData.lastName);
        await this.email.fill(formData.email);
        await this.mobilePhone.fill(formData.mobilePhone);
        await this.organization.fill(formData.organization);
        const availableCountryOptions = await this.country.evaluate((select) =>
        Array.from(select.options).map((option: any) => option.value));
        const countryValue = availableCountryOptions.includes(formData.country) ? formData.country : 'Germany';
        await this.country.selectOption(countryValue);
        await this.referralSource.fill(formData.referralSource);
        await this.messageBox.fill(formData.message);
        await this.marketingOffersCheckbox.check();
        await this.sendMessageButton.hover();
    };
};