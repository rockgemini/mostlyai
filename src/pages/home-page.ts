import type { Locator, Page } from '@playwright/test';

export class HomePage {
	readonly acceptCookieButton: Locator
	readonly platformBookmark: Locator
	readonly syntheticDataBookmark: Locator
	readonly resourcesBookmark: Locator
	readonly companyBookmark: Locator
	readonly pricingBookmark: Locator
	readonly searchLens: Locator
	readonly searchTextBox: Locator
	readonly contactLink: Locator

	constructor(readonly page: Page) {
		this.acceptCookieButton = page.locator('#CookieBoxSaveButton');
		this.platformBookmark = page.getByRole('button', { name: 'Platform' });
		this.syntheticDataBookmark = page.getByRole('button', { name: 'Synthetic Data' });
		this.resourcesBookmark = page.getByRole('button', { name: 'Resources' });
		this.companyBookmark = page.getByRole('button', { name: 'Company' });
		this.pricingBookmark = page.locator("//li[@class='oxy-mega-dropdown oxy-mega-dropdown_no-dropdown']//a[normalize-space()='Pricing']/span[@class='oxy-mega-dropdown_link-text']");
		this.searchLens = page.getByLabel('Open search');
		this.searchTextBox = page.getByPlaceholder('Search...');
		this.contactLink = page.locator("//div[contains(@class, 'ct-text-block') and text()='Contact']");

	};

	async load() {
		await this.page.goto('https://mostly.ai/');
		await this.page.waitForLoadState('load');
	};

	async performSearch(text: string) {
		await this.searchLens.click();
		await this.searchTextBox.waitFor({ state: 'visible' });
		await this.searchTextBox.fill(text);
		await this.searchTextBox.press('Enter');
	};

	async acceptCookie() {
		await this.acceptCookieButton.waitFor({ state: 'visible' });
		await this.acceptCookieButton.click();
	};	
};
