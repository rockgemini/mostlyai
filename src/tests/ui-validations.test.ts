import { expect, test } from 'src/fixtures/site-fixtures';
import { generateContactFormData } from 'src/helpers/common.helper';

test.describe('Mostly AI website checks', () => {
	test.beforeEach(async ({ homePage, page }) => {
		await homePage.load();
		await page.mouse.move(100, 100);
		await homePage.acceptCookie();
	});
	test('TC001 - Verify that bookmarks are visible', async ({ homePage }) => {
		await expect(homePage.platformBookmark).toBeVisible();
		await expect(homePage.syntheticDataBookmark).toBeVisible();
		await expect(homePage.resourcesBookmark).toBeVisible();
		await expect(homePage.companyBookmark).toBeVisible();
		await expect(homePage.pricingBookmark).toBeVisible();
	});

	test('TC002 - Verify text displayed when no matching search result is found', async ({ homePage, searchResultsPage }) => {
		const searchText = "sythetic";
		await homePage.performSearch(searchText);
		const expectedText = `Sorry, no results for: ${searchText}`
		const actualText = await searchResultsPage.returnSearchResultText();
		expect(actualText).toEqual(expectedText);
	});

	test('TC003 - Verify opening & filling the contact form', async ({ contactPage, homePage, page }) => {
		const formData = generateContactFormData();
		await homePage.companyBookmark.hover();
		await homePage.contactLink.click();
		expect(page.url()).toEqual(contactPage.url);
		await contactPage.fillContactForm(formData);
	});
});
