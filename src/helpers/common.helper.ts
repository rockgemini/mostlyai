import { FormData } from 'src/pages/contact-page';
import { faker } from '@faker-js/faker';

const countryMapping = {
  'Czechia': 'Czech Republic',
  'Democratic People\'s Republic of Korea': 'North Korea',
  'Libyan Arab Jamahiriya': 'Libya',
  'Russian Federation': 'Russia',
  'Svalbard & Jan Mayen Islands': 'Svalbard & Jan Mayen',
  'Syrian Arab Republic': 'Syria',
  'Virgin Islands, British': 'British Virgin Islands'  
};

export function generateContactFormData(): FormData {
	const fName = faker.person.firstName();
	const lName = faker.person.lastName();
  const fakerCountry = faker.location.country();
  const mappedCountry = countryMapping[fakerCountry] || fakerCountry;
  return {
    firstName: fName,
    lastName: lName,
    email: faker.internet.email({ firstName: fName, lastName: lName, provider: 'cosmos.io' }),
    mobilePhone: faker.phone.number('404-###-###'),
    organization: faker.company.name(),
    country: mappedCountry,
    referralSource: faker.lorem.word(),
    message: faker.lorem.paragraph(),
  };
};